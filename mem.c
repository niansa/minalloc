#include "mem.h"

#include <stdint.h>
#include <stddef.h>


struct memchunk_free {
    size_t size;
    struct memchunk_free *next;
};



struct memchunk_free heap = {
    .next = 0,
    .size = 0
};

void *mem_base;
size_t mem_size;
static size_t mem_used = 0;

inline static size_t size_align(size_t num) {
    return ((num + sizeof(struct memchunk_free) - 1) / sizeof(struct memchunk_free)) * sizeof(struct memchunk_free);
}

void heap_init() {
    heap.next = (struct memchunk_free *)mem_base;
    heap.next->size = mem_size;
    heap.next->next = 0;
}

void *heap_alloc(size_t *size) {
    // Allocations can't be smaller than this
    if(*size < sizeof(struct memchunk_free))
        *size = sizeof(struct memchunk_free);

    // Force-align size
    *size = size_align(*size);
    // Find first free space
    struct memchunk_free *nextmatch = heap.next, *baknext = &heap;

    // Get a chunk which fits this size
    while (nextmatch && nextmatch->size < *size) {
        baknext = nextmatch;
        nextmatch = nextmatch->next;
    }

    // If no chunk fits
    if(!nextmatch || nextmatch->size < *size) {
        return NULL;
    }

    // Calculate the remaining size of the chunk
    size_t new_size = nextmatch->size - *size;

    // If there is anything left, place it back in the list
    if(new_size >= sizeof(struct memchunk_free)) {
        struct memchunk_free *bak = baknext->next->next;
        baknext->next = (struct memchunk_free *)((size_t)nextmatch + *size);
        baknext->next->next = bak;
        baknext->next->size = new_size;
    } else {
        *size = nextmatch->size;
        baknext->next = nextmatch->next;
    }

    // Increase counter
    mem_used += *size;

    // Return allocated memory
    return nextmatch;
}

void *minmalloc(size_t size) {
    // Add space for size
    size += sizeof(uintptr_t);

    uint64_t *mem = (size_t *)heap_alloc(&size);

    if (!mem)
        return NULL;

    mem[0] = size;
    return mem + 1;
}

void heap_dealloc(void *memptr, size_t size) {
    // Convert this back into a chunk
    struct memchunk_free *chunk = (struct memchunk_free *)memptr;

    // Reinsert the chunk into the freelist
    chunk->size = size;
    chunk->next = heap.next;
    heap.next = chunk;

    // Decrease counter
    mem_used -= size;
}

void minfree(void *memptr) {
    if (!memptr)
        return;

    size_t *mem = (size_t *)memptr - 1;
    heap_dealloc(mem, mem[0]);
}

size_t get_mem_used() {
    return mem_used;
}
size_t get_mem_total() {
    return mem_size;
}
uint64_t get_mem_free() {
    return get_mem_total() - get_mem_used();
}
