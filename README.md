# Minalloc

A super minimal allocator. It doesn't dynamically allocate more system memory.

## Usage

You can use it by setting `mem_base` to the start of the memory you've allocated and `mem_size` to the length (in bytes). Then call `heap_init()`.

For more info, read [`include/mem.h`](/include/mem.h).

## Disclaimer

Minalloc isn't very efficient on memory, but the code size is quite low.
